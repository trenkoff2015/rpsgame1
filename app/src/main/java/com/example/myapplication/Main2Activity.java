package com.example.myapplication;

import android.content.pm.ActivityInfo;
import android.support.constraint.solver.widgets.Rectangle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.Random;

public class Main2Activity extends AppCompatActivity {

    Button b_rock, b_paper, b_scissors;
    ImageView iv_cpu, iv_me;

    String my_Choise, cpu_Choise, result;

    Random r;

    public abstract class BaseObject {

        public byte type;
        public byte state;
        public Rectangle body;

        public abstract void update(float delta);
        public abstract void draw(float delta);
    }

    public  interface Drawer {
        void draw(BaseObject obj, float delta);
    }

    public abstract class State {
        public byte type;
        public float x, y;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        iv_cpu = (ImageView) findViewById(R.id.iv_cpu);
        iv_me = (ImageView) findViewById(R.id.iv_me);

        b_rock = (Button) findViewById(R.id.rock);
        b_paper = (Button) findViewById(R.id.paper);
        b_scissors = (Button) findViewById(R.id.scissors);

        r = new Random();


        b_rock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                my_Choise = "rock";
                iv_me.setImageResource(R.drawable.rock);
                calculate ();
            }
        });
        b_paper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                my_Choise = "paper";
                iv_me.setImageResource(R.drawable.paper);
                calculate ();
            }
        });
        b_scissors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                my_Choise = "scissors";
                iv_me.setImageResource(R.drawable.scissors);
                calculate ();
            }
        });

    }

    public void calculate (){
        int cpu = r.nextInt(3);
        if(cpu == 0){
            cpu_Choise = "rock";
            iv_cpu.setImageResource(R.drawable.rock);
        } else if(cpu == 1) {
            cpu_Choise = "paper";
            iv_cpu.setImageResource(R.drawable.paper);
        } else if(cpu == 2) {
            cpu_Choise = "scissors";
            iv_cpu.setImageResource(R.drawable.scissors);
        }

        if(my_Choise.equals("rock") && cpu_Choise.equals("paper")){
            result = "ты проиграл";
        } else
        if(my_Choise.equals("rock") && cpu_Choise.equals("scissors")) {
            result = "ты выиграл";
        } else
        if(my_Choise.equals("paper") && cpu_Choise.equals("rock")){
            result = "ты выиграл";
        } else
        if(my_Choise.equals("paper") && cpu_Choise.equals("scissors")) {
            result = "ты проиграл";
        } else
        if(my_Choise.equals("scissors") && cpu_Choise.equals("paper")){
            result = "ты выиграл";
        } else
        if(my_Choise.equals("scissors") && cpu_Choise.equals("rock")) {
            result = "ты проиграл";
        } else
        if(my_Choise.equals("scissors") && cpu_Choise.equals("scissors")) {
            result = "ничья";
        } else
        if(my_Choise.equals("paper") && cpu_Choise.equals("paper")) {
            result = "ничья";
        } else
        if(my_Choise.equals("rock") && cpu_Choise.equals("rock")) {
            result = "ничья";
        }

        Toast.makeText(Main2Activity.this, result, Toast.LENGTH_SHORT).show();
    }
}
