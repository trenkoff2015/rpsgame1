package com.example.myapplication;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import static android.R.id.message;

public class MainActivity extends AppCompatActivity {

    public final static String EXTRA_MESSAGE = "EXTRA_MESSAGE";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    public void sendMessage(View view) {

        Intent intent = new Intent(this, Main2Activity.class);

        intent.putExtra(EXTRA_MESSAGE, message);

        startActivity(intent);
    }

    public void bluetoothMessage(View view) {

        Intent intent = new Intent(this, BluetoothActivity.class);

        intent.putExtra(EXTRA_MESSAGE, message);

        startActivity(intent);
    }
}
